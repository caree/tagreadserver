require("should");
var _ = require("underscore");

var udpServer = require('../routes/udpServer');

var rawInfo = "Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00002, Ant:02, Type:04, Tag:300833B2DDD906C001010101   ";
var longRawInfo = 'Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:03, Type:04, Tag:300833B2DDD906C001010102  \r\n Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:02, Type:04, Tag:300833B2DDD906C001010103   \r\n Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:02, Type:04, Tag:300833B2DDD906C001010101    ';

Array.prototype.mapMinusSelf = function(){
    if(this.length <= 1) return [];
    var mapMinus = [];
    for(var i = 0, len = this.length; i < len -1; i++){
        mapMinus.push(this[i] - this[i+1]);
    }

    return mapMinus;
}
describe("Parse => ", function() {
    it("This parse an item", function() {
        udpServer.clearTempTagList();
        var tag = udpServer.parseSingleItem(rawInfo);
        (tag.epc == '300833B2DDD906C001010101').should.be.true;
        (tag.antennaID == '02').should.be.true;
        (tag.readCount == 2).should.be.true;
    });

    it("this would get a list", function(){
        udpServer.clearTempTagList();

        var tagList = udpServer.parseMultiItem(longRawInfo);
        console.dir(tagList);
        (_.size(tagList) == 3).should.be.true;
        tagList = udpServer.parseMultiItem(longRawInfo);
        console.dir(tagList);
        (_.size(tagList) == 6).should.be.true;

        var groupedList = _.groupBy(tagList, function(_tag){
            return _tag.epc;
        });
        console.dir(groupedList);
        _.each(groupedList, function(_group){
            var sortedList = _.sortBy(_group, function(_tag){
                return -_tag.timeStamp;
            });
            console.dir(sortedList);
        })
    });

    it("this test mapMinusSelf => ", function(){
        var a = [15,9,7,6,1];
        var map = a.mapMinusSelf();
        console.dir(map);
        (_.size(map) == 4).should.be.true;
    });

    it('test uniq => ', function(){
        var list = [{name:'111', age:1}, {name:'222', age:11}, {name:'333', age:10}, {name:'222', age:13}];
        var uniqList = _.uniq(list, false, function(_obj){
            return _obj.name;
        });
        console.dir(uniqList);
    });
    it('split string => ', function(){
        var str = '300833B2DDD906C00101010209';
        var epc = str.substr(0, 24);
        var ant = str.substr(24, 2);
        (epc == '300833B2DDD906C001010102').should.be.true;
        (ant == '09').should.be.true;
    });
    it('set bool => ', function(){
        var bTrue = true;
        var bFalse = !bTrue;
        (bFalse == false).should.be.true;
    });

});


